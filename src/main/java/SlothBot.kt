import common.Command
import common.Constants
import common.KeyboardCommand
import services.GuessNumberGameService.guessNumber
import services.GuessNumberGameService.showLeaders
import services.GuessNumberGameService.startGame
import services.CommandService.getBtcExchangeRate
import services.CommandService.getIssLocation
import services.CommandService.getJoke
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.TelegramBotsApi
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Message
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession
import services.CommandService.sayHello
import java.util.*

fun main() {
    TelegramBotsApi(DefaultBotSession::class.java).registerBot(SlothBot())
}

class SlothBot : TelegramLongPollingBot() {
    override fun getBotUsername() = getProp(Constants.BOT_USERNAME)
    override fun getBotToken() = getProp(Constants.BOT_TOKEN)

    companion object {
        private var instance = SlothBot()
        fun getInstance(): SlothBot = instance
    }


    override fun onUpdateReceived(update: Update) {
        if (update.hasMessage()) {
            update.message.text = update.message.text.removeSuffix(Constants.suffixToRemove)
            processMessageText(update)
        }
        if (update.hasCallbackQuery()) {
            processCallbackQuery(update)
        }
    }

    private fun processCallbackQuery(update: Update) {
        when (update.callbackQuery.data) {
            KeyboardCommand.BTC -> getBtcExchangeRate(update)
            KeyboardCommand.JOKE -> getJoke(update)
            KeyboardCommand.ISS -> getIssLocation(update)
        }
    }

    private fun processMessageText(update: Update) {
        when (update.message.text as Any) {
            Command.START -> sayHello(update)
            Command.BTC -> getBtcExchangeRate(update)
            Command.JOKE -> getJoke(update)
            Command.ISS -> getIssLocation(update)
            Command.MENU -> getInlineKeyboard(update)
            Command.GAME -> startGame(update)
            Command.LEADERS -> showLeaders(update)
            is String -> guessNumber(update)
        }
    }

    fun sayText(update: Update, text: String): Message = execute(SendMessage(getMessageChatId(update).toString(), text))

    fun getMessageChatId(update: Update) = update.message?.chatId?.toString()

    fun getCallbackQueryChatId(update: Update) = update.callbackQuery.message.chatId.toString()

    private fun getInlineKeyboard(update: Update) {
        execute(
            SendMessage.builder()
                .chatId(getMessageChatId(update).toString())
                .text("Чего изволите?")
                .replyMarkup(InlineKeyboardMarkup().apply {
                    keyboard = listOf(
                        listOf(
                            InlineKeyboardButton(KeyboardCommand.BTC.uppercase()).apply {
                                callbackData = KeyboardCommand.BTC
                            },
                            InlineKeyboardButton(KeyboardCommand.JOKE.uppercase()).apply {
                                callbackData = KeyboardCommand.JOKE
                            },
                            InlineKeyboardButton(KeyboardCommand.ISS.uppercase()).apply {
                                callbackData = KeyboardCommand.ISS
                            })
                    )
                }).build()
        )
    }

    private fun getProp(prop: String): String =
        javaClass.classLoader.getResourceAsStream(Constants.BOT_PROP).use { Properties().apply { load(it) } }
            .getProperty(prop)
}
