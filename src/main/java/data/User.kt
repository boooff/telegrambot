package data

data class User(val id: Long, val firstName: String?, val lastName: String?, val userName: String?)
