package data

data class Game (val user: User, val chatId: String, var guessCounter: Int = 0, val guess: Int)