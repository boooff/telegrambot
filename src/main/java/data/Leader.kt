package data

data class Leader(val user: User, val attempts: Int) {
    override fun toString(): String {
        return "${user.firstName ?: "-"} ${user.userName ?: ""} ${user.lastName ?: ""} - $attempts"
    }
}