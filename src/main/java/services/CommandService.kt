package services

import common.ConstantUrl
import SlothBot
import org.json.JSONObject
import org.telegram.telegrambots.meta.api.methods.send.SendLocation
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Update
import java.net.URL
import java.nio.charset.Charset

object CommandService {
    private val slothBot = SlothBot.getInstance()

    fun sayHello(update: Update) {
        slothBot.execute(SendMessage(slothBot.getMessageChatId(update)!!, "Hello, ${update.message.from.firstName}!"))
    }

    fun getIssLocation(update: Update) {
        val issLocation = ConstantUrl.ISS.getJSONData()["iss_position"] as HashMap<*, *>

        slothBot.execute(
            SendMessage(
                slothBot.getMessageChatId(update) ?: slothBot.getCallbackQueryChatId(update),
                "МКС сейчас здесь:"
            )
        )
        slothBot.execute(
            SendLocation(
                slothBot.getMessageChatId(update) ?: slothBot.getCallbackQueryChatId(update),
                issLocation["latitude"].toString().toDouble(),
                issLocation["longitude"].toString().toDouble()
            )
        )
    }

    fun getJoke(update: Update) {
        val joke = ConstantUrl.JOKE.getJSONData()
        slothBot.execute(
            SendMessage(
                slothBot.getMessageChatId(update) ?: slothBot.getCallbackQueryChatId(update),
                "${joke["content"]}"
            )
        )
    }

    fun getBtcExchangeRate(update: Update) {
        val exchangeRate = ConstantUrl.BTC.getJSONData()
        slothBot.execute(
            SendMessage(
                slothBot.getMessageChatId(update) ?: slothBot.getCallbackQueryChatId(update),
                "Курс биткоина: ${exchangeRate["USD"]} USD (${exchangeRate["RUB"]} RUB)"
            )
        )
    }

    private fun String.getJSONData(): Map<String, Any> {
        val jsonData = URL(this).readText(Charset.forName("cp1251"))
        return try {
            JSONObject(jsonData).toMap()
        } catch (e: Exception) {
            return mapOf("content" to jsonData.removePrefix("{\"content\":\"").removeSuffix("\"}"))
        }
    }
}