package services

import SlothBot
import common.Constants
import data.Game
import data.Leader
import services.CommandService.sayHello
import data.User
import org.telegram.telegrambots.meta.api.objects.Update
import kotlin.random.Random
import org.slf4j.Logger
import org.slf4j.LoggerFactory

object GuessNumberGameService {
    private val slothBot = SlothBot.getInstance()
    private val log: Logger = LoggerFactory.getLogger(this.javaClass)
    private var activeGames = mutableListOf<Game>()
    private var leaderBoard = mutableListOf<Leader>()

    internal fun guessNumber(update: Update) {
        if (activeGames.isEmpty() || !activeGames.contains(activeGames.find { it.user.id == update.message.from.id }))
            return

        val userGuess = update.message.text

        if (userGuess == "end") {
            activeGames.remove(activeGames.find { it.user.id == update.message.from.id })
            log.info("User ${update.message.from.userName} canceled the game")
            slothBot.sayText(update, "Вы вышли из игры")
            return
        }
        if (userGuess.length != 4 || userGuess.toIntOrNull() == null) {
            log.error("$userGuess is not a number or the length is not 4")
            slothBot.sayText(update, "Введите 4-значное число")
            return
        }
        checkGuess(update, userGuess)
    }

    internal fun showLeaders(update: Update) {
        if (leaderBoard.isEmpty()) {
            slothBot.sayText(update, "Пока никто не играл =(")
            return
        }

        leaderBoard.sortBy { it.attempts }
        leaderBoard.forEach { slothBot.sayText(update, it.toString()) }

    }

    private fun checkGuess(update: Update, userGuess: String) {
        val game = activeGames.find { it.user.id == update.message.from.id }
        log.info("userGuess: $userGuess [$game]")
        if (game != null) {
            game.guessCounter++
            val result = getGuessResult(userGuess, game.guess.toString())
            slothBot.sayText(update, result)
            if (result == Constants.WIN_RESULT) {
                slothBot.sayText(
                    update,
                    "${game.user.userName}, победа, победа вместо обеда!!! Попыток потребовалось: ${game.guessCounter}"
                )
                updateLeaders(game)
                activeGames.remove(game)
            }
            return
        }

        log.error("Current user's game is not found")
        slothBot.sayText(update, "Игра пользователя не найдена!")
    }

    private fun getGuessResult(userGuess: String, actual: String): String {
        var res = ""
        val userG = userGuess.toMutableList()
        actual.forEachIndexed { index, c ->
            if (userG[index] == c) {
                res += 'B'
                userG[index] = '-'
            } else if (userG.contains(c)) {
                res += 'K'
                userG[userG.indexOf(c)] = '-'
            }
        }
        return if (res.isNotEmpty())
            res.toCharArray().sorted().joinToString("")
        else "X"
    }

    private fun updateLeaders(game: Game) {
        if (leaderBoard.size < 10) {
            leaderBoard.add(Leader(game.user, game.guessCounter))
            return
        }

        leaderBoard.sortBy { it.attempts }
        if (leaderBoard.last().attempts > game.guessCounter) {
            leaderBoard.removeLast()
            leaderBoard.add(Leader(game.user, game.guessCounter))
        }

        leaderBoard.forEach { log.info(it.toString()) }
    }

    internal fun startGame(update: Update) {
        if (activeGames.find { update.message.from.id == it.user.id } != null) {
            slothBot.sayText(update, "Для данного пользователя найдена активная игра. Для её завершения набери 'end'")
            return
        }
        sayHello(update)
        val user = User(
            update.message.from.id,
            update.message.from?.firstName,
            update.message.from?.lastName,
            update.message.from?.userName
        )
        activeGames.add(Game(user, slothBot.getMessageChatId(update).toString(), guess = Random.nextInt(1000, 10000)))
        slothBot.sayText(
            update,
            "${user.userName}, я загадал 4-значное число, попробуй угадай. Цифры могут повторяться. " +
                    "Если правильно угадано место цифры, то в ответе будет 'B', в противном случае - 'K'. " +
                    "Если в числе не встречается ни одна из цифр - 'X'. Для завершения игры набери 'end'"
        )
        log.info("$user has started a new game")
    }
}
