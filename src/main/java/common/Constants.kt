package common

object Constants {
    const val BOT_TOKEN = "BOT_TOKEN"
    const val BOT_USERNAME = "BOT_USERNAME"

    const val BOT_PROP = "bot.properties"

    const val suffixToRemove = "@OldSlothDuploBot"

    const val WIN_RESULT = "BBBB"
}