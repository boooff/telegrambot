package common

object Command {
    const val START = "/start"
    const val BTC = "/btc"
    const val JOKE = "/joke"
    const val ISS = "/iss"
    const val MENU = "/menu"
    const val GAME = "/game"
    const val LEADERS = "/leaders"
}